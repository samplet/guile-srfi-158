;;; Guile SRFI 158
;;; Copyright © 2019 Timothy Sample <samplet@ngyro.com>
;;;
;;; This file is part of Guile SRFI 158.
;;;
;;; Guile SRFI 158 is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; Guile SRFI 158 is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Guile SRFI 158.  If not, see
;;; <https://www.gnu.org/licenses/>.

(define-module (srfi srfi-158)
  #:use-module (rnrs bytevectors)
  #:use-module ((rnrs io simple) #:select (eof-object))
  #:use-module (srfi srfi-11)
  #:export (generator
            circular-generator
            make-iota-generator
            make-range-generator
            make-coroutine-generator
            list->generator
            vector->generator
            reverse-vector->generator
            string->generator
            bytevector->generator
            make-for-each-generator
            make-unfold-generator

            gcons*
            gappend
            gcombine
            gfilter
            gremove
            gtake
            gdrop
            gtake-while
            gdrop-while
            gflatten
            ggroup
            gmerge
            gmap
            gstate-filter
            gdelete
            gdelete-neighbor-dups
            gindex
            gselect

            generator->list
            generator->reverse-list
            generator->vector
            generator->vector!
            generator->string
            generator-fold
            generator-map->list
            generator-for-each
            generator-find
            generator-count
            generator-any
            generator-every
            generator-unfold

            make-accumulator
            count-accumulator
            list-accumulator
            reverse-list-accumulator
            vector-accumulator
            reverse-vector-accumulator
            vector-accumulator!
            string-accumulator
            bytevector-accumulator
            bytevector-accumulator!
            sum-accumulator
            product-accumulator))

(include-from-path "srfi/srfi-158/srfi-158-impl.scm")
