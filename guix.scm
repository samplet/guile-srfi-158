(use-modules (gnu packages autotools)
             (gnu packages guile)
             (gnu packages pkg-config)
             (guix build-system gnu)
             ((guix licenses) #:prefix license:)
             (guix packages))

(package
  (name "guile-srfi-158")
  (version "1.1.0")
  (source #f)
  (build-system gnu-build-system)
  (native-inputs
   `(("autoconf" ,autoconf)
     ("automake" ,automake)
     ("pkg-config" ,pkg-config)))
  (inputs
   `(("guile" ,guile-2.2)))
  (home-page "https://gitlab.com/samplet/guile-srfi-158")
  (synopsis "SRFI 158 (Generators and Accumulators) for Guile")
  (description "This package provides an implementation of SRFI 158
for Guile.  SRFI 158 defines utility procedures that create,
transform, and consume generators.  It also defines procedures that
return accumulators.  It is implemented by wrapping the sample
implementation in a thin Guile compatibility layer.")
  (license license:gpl3+))
