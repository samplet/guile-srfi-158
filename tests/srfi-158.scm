;;; Guile SRFI 158
;;; Copyright © 2019 Timothy Sample <samplet@ngyro.com>
;;;
;;; This file is part of Guile SRFI 158.
;;;
;;; Guile SRFI 158 is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; Guile SRFI 158 is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Guile SRFI 158.  If not, see
;;; <https://www.gnu.org/licenses/>.

(use-modules (rnrs bytevectors)
             ((rnrs io simple) #:select (eof-object))
             ((srfi srfi-1) #:select (unfold))
             (srfi srfi-11)
             (srfi srfi-64)
             (srfi srfi-158))

(define-syntax-rule (cond-expand x ...) (noop))

(define (bytevector . args)
  (let* ((len (length args))
         (bv (make-bytevector len)))
    (for-each (lambda (i x) (bytevector-u8-set! bv i x)) (iota len) args)
    bv))

(define-syntax test
  (syntax-rules ()
    ((_ name expect expr)
     (test-equal name expect expr))
    ((_ expect expr)
     (test (with-output-to-string (lambda () (write 'expr))) expect expr))))

(define-syntax work-around-define
  (syntax-rules (define)
    ((_ (define (name arg ... . rest) body ...))
     (module-define! (current-module) 'name
                     (lambda (arg ... . rest) body ...)))
    ((_ (define name value))
     (module-define! (current-module) 'name value))
    ((_ x) x)))

(define-syntax-rule (test-group name-expr body ...)
  ((@ (srfi srfi-64) test-group) name-expr (work-around-define body) ...))

(define-syntax-rule (test-exit) (noop))

(test-begin "srfi-158")

(include "srfi-158/chicken-test.scm")

(test-end)

(let ((runner (test-runner-current)))
  (quit (and (zero? (test-runner-fail-count runner))
             (zero? (test-runner-xpass-count runner)))))
